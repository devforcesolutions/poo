
public class Divano extends Oggetto {

	private double lunghezza;

	public Divano(String nome, double peso, double lunghezza) {
		super(nome, peso);
		this.lunghezza = lunghezza;
	}

	public double getLunghezza() {
		return lunghezza;
	}

	public void setLunghezza(double lunghezza) {
		this.lunghezza = lunghezza;
	}
	
	
}
