package polimorfismo.classeastratta;

public abstract class Veicolo {
	protected Colore colore;
	protected int velMax;
	protected int numCav;
	protected int velocitaAttuale;
	
	protected String targa;
	
	
	protected int litriBenzina;
	protected int pasticche;
	
	
	public Veicolo(Colore colore, int velMax, int numCav, String targa, int litriBenzina, int pasticche) {
		this.colore = colore;
		this.velMax = velMax;
		this.numCav = numCav;
		this.targa = targa;
		this.velocitaAttuale = 0;
		this.litriBenzina = litriBenzina;
		this.pasticche = pasticche;
	}


	public Colore getColore() {
		return colore;
	}


	public void setColore(Colore colore) {
		this.colore = colore;
	}


	public int getVelMax() {
		return velMax;
	}


	public void setVelMax(int velMax) {
		this.velMax = velMax;
	}


	public int getNumCav() {
		return numCav;
	}


	public void setNumCav(int numCav) {
		this.numCav = numCav;
	}
	
	
	public int getVelocitaAttuale() {
		return velocitaAttuale;
	}


	public void setVelocitaAttuale(int velocitaAttuale) {
		this.velocitaAttuale = velocitaAttuale;
	}


	public String getTarga() {
		return targa;
	}


	public void setTarga(String targa) {
		this.targa = targa;
	}


	public int getLitriBenzina() {
		return litriBenzina;
	}


	public void setLitriBenzina(int litriBenzina) {
		this.litriBenzina = litriBenzina;
	}


	public int getPasticche() {
		return pasticche;
	}


	public void setPasticche(int pasticche) {
		this.pasticche = pasticche;
	}
	
	
	public abstract boolean accendi();
	public abstract void spegni();
	public abstract boolean accellera();
	public abstract void frena();
	public abstract void aggiungiLitri();
}
	