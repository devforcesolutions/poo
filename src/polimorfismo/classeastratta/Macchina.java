package polimorfismo.classeastratta;

public class Macchina extends Veicolo {
	private MarcaAuto marca;
	
	
	public static final int SERBATOIO = 60;
	
	
	public Macchina(MarcaAuto marca, int velMax, int numCav, String targa, Colore colore) {
		super(colore, velMax, numCav, targa, 20, 100);
		this.marca = marca;
	}
	
	public MarcaAuto getMarca() {
		return marca;
	}
	public void setMarca(MarcaAuto marca) {
		this.marca = marca;
	
	}

	
	@Override
	public boolean accendi() {
		
		if (this.litriBenzina > 0) {
			System.out.println("la macchina si � accesa ");
			return true;
		}
		else { 
			System.out.println("non c'� benzina ");
			return false;		
		}
		
	}
	@Override
	public void spegni() {
		this.velocitaAttuale = 0;
		System.out.println("la macchina si � spenta ");
	}
	
	@Override
	public boolean accellera() {
		int newVel = this.velocitaAttuale + 20; 
		int newLitri = this.litriBenzina - 2;
		
		if (newVel > this.velMax) {
			return false;
		}
		else if (newLitri <= 0) {
			this.spegni();
			return false;
		}
		else {
			this.velocitaAttuale = newVel;
			this.litriBenzina = newLitri;
			System.out.println("stai viaggiando a " + this.velocitaAttuale + " km/h e hai ancora " + this.litriBenzina + " litri di broda");
			return true;
			
		}
	}
	@Override
	public void frena() {
		if (this.pasticche >= 5) {
			this.velocitaAttuale = this.velocitaAttuale - 20;
			this.pasticche = this.pasticche - 5;
			System.out.println("stai viaggiando a " + this.velocitaAttuale + " km/h e hai ancora " + this.litriBenzina + " litri di broda");

		
		}
		else {
			System.out.println("nun poe frena");
			
		}
	}
	@Override
	public void aggiungiLitri() {
		
		int newLitri = this.litriBenzina + 5;
		
		if(newLitri <= Macchina.SERBATOIO ) {
			this.litriBenzina = newLitri;
			
			
		} else {
			this.litriBenzina = Macchina.SERBATOIO;
				}
			
		
	}
	

}
