package polimorfismo.classeastratta;

public class Moto extends Veicolo {
	
	private MarcaMoto marca;
	
	public static final int SERBATOIO = 40; 
	

	
	public MarcaMoto getMarca() {
		return marca;
	}

	public void setMarca(MarcaMoto marca) {
		this.marca = marca;
	}

	public static int getSerbatoio() {
		return SERBATOIO;
	}

	public Moto(Colore colore, int velMax, int numCav, String targa, int litriBenzina, int pasticche, MarcaMoto marca) {
		super(colore, velMax, numCav, targa, litriBenzina, pasticche);
		this.marca = marca;
	}

	@Override
	public boolean accendi() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void spegni() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean accellera() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void frena() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void aggiungiLitri() {
		// TODO Auto-generated method stub
			}
		
	
}
