package polimorfismo.interfacce;

public class Pianoforte implements Strumento<SuonoPianoforte> {
	private String colore;
	private String marca;
	private int tasti;
	
	
	public Pianoforte(String colore, String marca, int tasti) {
		this.colore = colore;
		this.marca = marca;
		this.tasti = tasti;
	}


	public String getColore() {
		return colore;
	}


	public void setColore(String colore) {
		this.colore = colore;
	}


	public String getMarca() {
		return marca;
	}


	public void setMarca(String marca) {
		this.marca = marca;
	}


	public int getTasti() {
		return tasti;
	}


	public void setTasti(int tasti) {
		this.tasti = tasti;
	}


	@Override
	public SuonoPianoforte suono() {
		return new SuonoPianoforte();
	}


	

}
