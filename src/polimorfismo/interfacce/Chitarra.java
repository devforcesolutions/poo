package polimorfismo.interfacce;

public class Chitarra implements Strumento<SuonoChitarra> {
	
	private TipoModello modello;
	private String marca;
	private String colore;
	public Chitarra(TipoModello modello, String marca, String colore) {

		this.modello = modello;
		this.marca = marca;
		this.colore = colore;
	}
	public TipoModello getModello() {
		return modello;
	}
	public void setModello(TipoModello modello) {
		this.modello = modello;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getColore() {
		return colore;
	}
	public void setColore(String colore) {
		this.colore = colore;
	}
	
    @Override
	public SuonoChitarra suono() {
    	return new SuonoChitarra();
	}
}
