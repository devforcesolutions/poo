package polimorfismo.interfacce;

public class Bongo implements Strumento<SuonoBongo> {

	private String marca;

	public Bongo(String marca) {
		this.marca = marca;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	@Override
	public SuonoBongo suono() {
		return new SuonoBongo();
	}
	
	
	
}
