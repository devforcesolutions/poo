package polimorfismo.interfacce;

import ereditarieta.Persona;

public class Musicista extends Persona {
	
	
	
	public Musicista(String nome, String cognome, int eta) {
		super(nome, cognome, eta);
	}
	
	
	public void suonaStrumento(Strumento s) {
		s.suono();
	}

}
