package polimorfismo.interfacce;

public interface Strumento<T> {
	
	public T suono();
	
}
