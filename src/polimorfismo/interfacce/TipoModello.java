package polimorfismo.interfacce;

public enum TipoModello {
	
	CHITARRA_ELETTRICA("Chitarra elettrica"),
	CHTIARRA_ACUSTICA("Chitarra acustica"),
	CHITARRA_CLASSICA("Chitarra classica");
	
	private String descrizione;
	
	TipoModello(String descrizione) {
		this.descrizione = descrizione;
	}
	
	public String getDescrizione() {
		return this.descrizione;
	}

}
