package polimorfismo.interfacce;

public class Main {

	public static void main(String[] args) {
		Musicista m = new Musicista("Ludovico","Enaudi",33);
		
		Strumento chitarra = new Chitarra(TipoModello.CHITARRA_CLASSICA, "Martin", "Marrone");
		Chitarra chitarraNonStrunmentata = new Chitarra(TipoModello.CHTIARRA_ACUSTICA, "Martin", "Marrone");
		
		m.suonaStrumento(chitarra);
		m.suonaStrumento(chitarraNonStrunmentata);
	}
}
