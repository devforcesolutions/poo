
public class Doccia extends Oggetto {
	
	private boolean aperta;

	public Doccia(String nome, double peso) {
		super(nome, peso);
		this.aperta = false;
	}
	
	public boolean apri(){
		
		this.aperta = true;
		return aperta;
		}
	
	public boolean chiudi() {
		this.aperta = false;
		return aperta;
	}
	
	
	

}
