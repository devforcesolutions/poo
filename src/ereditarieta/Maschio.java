package ereditarieta;

public class Maschio extends Persona {
	

	
	public Maschio(String nome, String cognome, int eta, int lunghezzaPisello) {
		super(nome, cognome, eta);
		this.lunghezzaPisello = lunghezzaPisello;
	}

	private int lunghezzaPisello;

	public int getLunghezzaPisello() {
		return lunghezzaPisello;
	}

	public void setLunghezzaPisello(int lunghezzaPisello) {
		this.lunghezzaPisello = lunghezzaPisello;
	}
	
	
	
}
