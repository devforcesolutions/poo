package ereditarieta;

public class Persona {
	
	//Variabili d'istanza
	protected String nome;
	protected String cognome;
	protected int eta;
	
	protected static int numBraccia = 2;	//static=vale x ttt le istanze class persona. final= non puo essere mod in fase di esecuzione
	
	public Persona(String nome, String cognome, int eta) {
		this.nome = nome;
		this.cognome = cognome;
		this.eta = eta;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public int getEta() {
		return eta;
	}

	public void setEta(int eta) {
		this.eta = eta;
	}
	
	
	
	
	
	
	
	
}
