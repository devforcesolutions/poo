
public class Televisore extends Oggetto {
	
	private int pollici;
	private boolean acceso;
	
	

	public Televisore(String nome, double peso, int pollici) {
		super(nome, peso);
		this.pollici = pollici;
		this.acceso = false;
	}

	public int getPollici() {
		return pollici;
	}

	public void setPollici(int pollici) {
		this.pollici = pollici;
	}


	public boolean accendi() {
		this.acceso = true;
		return acceso;
	}

	
	public boolean spegni() {
		this.acceso = false;
		return acceso;
	}

}
