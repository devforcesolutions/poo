package collections;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ereditarieta.Persona;

public class EsercizioListe {
	
	public static void main(String[] args) {
		
		Persona[] arrayPersone = new Persona[30];
		
		List<Persona> listaPersone = new ArrayList<>();
		
		
		Persona p1 = new Persona("Marco", "Convalle", 24);
		Persona p2 = new Persona("Marco", "Carta", 30);
		
		arrayPersone[0] = p1;
		
		listaPersone.add(p1);
		listaPersone.get(0);
		
		Set<Persona> insiemePersone = new HashSet<>();
		
		insiemePersone.add(p1);
		insiemePersone.add(p2);
		
		
		Map<Long, Persona> mappaPersone = new HashMap<>();
		
		mappaPersone.put(1L, p1);
		mappaPersone.put(2L, p2);

		Persona temp = mappaPersone.get(1L);
		temp.setNome("Andrea");
		mappaPersone.put(1L, temp);
		
		
		Map<String, List<Persona>> mappaSquadre = new HashMap<>();
		
		List<Persona> listaGiocatoriRoma = new ArrayList();
		listaGiocatoriRoma.add(new Persona("Francesco", "Totti", 41));

		mappaSquadre.put("Roma", listaGiocatoriRoma);
	}

}
