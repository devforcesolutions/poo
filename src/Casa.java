import java.util.ArrayList;
import java.util.List;

import ereditarieta.Persona;
//import java.util.Map;

//import ereditarieta.Persona;

public class Casa {

	private List<Stanza> stanze;	//Se una stanza non c'� � perch� non l'ho inserita qui!
//	private Map<Stanza, List<Persona>> personeInStanze;
	private String nome;


	public Casa(String nome) {
		this.stanze = new ArrayList<>();
		this.nome = nome;
	}


	public List<Stanza> getStanze() {
		return stanze;
	}


	public void setStanze(List<Stanza> stanze) {
		this.stanze = stanze;
	}


	public String getNome() {
		return nome; 
	}


	public void setNome(String nome) {
		this.nome = nome;
	}
    
	
	public void accogli(String nomeStanza, Persona p) {
		Stanza stanzaDoveAccogliere = this.getStanzaByNome(nomeStanza);
				
		stanzaDoveAccogliere.getPersoneNellaStanza().add(p);
	}
		

	public Stanza getStanzaByNome(String s) {
		Stanza stanzaTrovata = null;
		for (Stanza stanza : this.stanze) {
			if (stanza.getNome().equals(s)) {
				stanzaTrovata = stanza;
			}
		}
		return stanzaTrovata;
	}
		
		
	/*//si ma a pensacce � sbagliato, lo stavo a vede mo...
	public boolean accogli(Persona p, Stanza s) {

		//Allora mi chiedo, quali persone potrebbero entrare nella tua casa? potenzialmente chiunque nel mondo? ok
		if (this.stanze.contains(s)) {	//Si chiede se la stanza s � contenuto nella lista stanze
			if (this.personeInStanze.get(s) == null) {
				this.personeInStanze.put(s, new ArrayList<>());	//Se la lista delle persone in quella stanza � null la istanzio
			}
			if (Mondo.esistePersonaNelMondo(p)) {
				 this.personeInStanze.get(s).add(p);
				 return true;//this.personeInStanze.get(s)	//� la lista appena istanziata e gli aggiungo p
			}
			else {
				System.out.println("La persona " + p.getNome() + " " + p.getCognome() + " non esiste nel mondo");
				return false;
			}
		}
		else {
			//Su che base puoi dire se la persona non esiste? che p � null? ma perch� io il controllo lo faccio sulla lista di stanze nella casa
			System.out.println("La stanza " + s.getNome() +" non � presente nella casa, impossibile inserire la persona " + p.getNome() + " " + p.getCognome());
			return false;
		}
		
	}
*/

  
}
