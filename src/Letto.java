
public class Letto extends Oggetto{
	
	

	private DimensioneLetto dimensione;



	public Letto(String nome, double peso, DimensioneLetto dimensione) {
		super(nome, peso);
		this.dimensione = dimensione;
	}
	
	public DimensioneLetto getDimensione() {
		return dimensione;
	}

	public void setDimensione(DimensioneLetto dimensione) {
		this.dimensione = dimensione;
	}
	
}
