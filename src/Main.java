import java.util.ArrayList;

import ereditarieta.Persona;

public interface Main {

	public static void main(String[] args) {
		
		Casa c = new Casa("Casa_Convalle");
		
		Oggetto televisione = new Televisore("Samsung", 40, 50);
		Oggetto televisione2 = new Televisore("Samsung", 40, 50);
		Oggetto divano = new Divano("Divano", 100, 3);
		Oggetto letto = new Letto ("Letto_Marco", 60, DimensioneLetto.MATRIMONIALE);
		Oggetto forno = new Forno("Delonghi", 40);
		Oggetto doccia = new Doccia("Doccia", 80);
		
		Stanza s1 = new Stanza ("Cucina", forno);
		Stanza s2 = new Stanza ("Bagno", doccia);
		Stanza s3 = new Stanza ("Salotto", divano, televisione);
		Stanza s4 = new Stanza ("Cameretta", letto, televisione2);
		
		//Te lo ricordi si, dipende, vediamo dentro il costruttore come funziona....
		
		
		s3.getCoordinataToStanzaAdiacente().put(Coordinata.SUD, s1);
		s3.getCoordinataToStanzaAdiacente().put(Coordinata.EST, s4);
		s4.getCoordinataToStanzaAdiacente().put(Coordinata.SUD, s2);
		s1.getCoordinataToStanzaAdiacente().put(Coordinata.EST, s2);
		
		s1.getCoordinataToStanzaAdiacente().put(Coordinata.NORD, s3);
		s4.getCoordinataToStanzaAdiacente().put(Coordinata.OVEST, s3);
		s2.getCoordinataToStanzaAdiacente().put(Coordinata.NORD, s4);
		s2.getCoordinataToStanzaAdiacente().put(Coordinata.OVEST, s1);
		
		
		//Esempio, se faccio...
		//Stanza stanzaASudDelSalotto = s3.getStanzaAdiacente(Coordinata.SUD);
		//stanzaASudDelSalotto.getNome(); 
		s3.getStanzaAdiacente(Coordinata.SUD).getNome();
	
		
		//Si dice che il metodo � parametrico... ovvero a seconda del parametro che gli passi (in questo caso la coordinata) ti restituisce un oggetto diverso
		
	
		
		Persona p2 = new Persona("Andrea", "Convalle", 30);
		Persona p3 = new Persona("MariaElena", "Convalle", 26);
		Persona p4 = new Persona("Luca", "Convalle", 34);
		Persona p5 = new Persona("Monica", "Convalle", 33);
		Persona p6 = new Persona("Marta", "Convalle", 36);
		Persona pStupida = new Persona("Marco", "Convalle", 24);
		
		Mondo.aggiungiPersona(pStupida);
		Mondo.aggiungiPersona(p2);
		Mondo.aggiungiPersona(p3);
		Mondo.aggiungiPersona(p4);
		Mondo.aggiungiPersona(p5);
		Mondo.aggiungiPersona(p6);
		
		
		//Niente ho preteso troppo anche stavolta... :(
		//c.accogli(pStupida, c.getStanze().get(0));
		
		
		
		
		c.accogli("Cucina", pStupida);
	}

}
