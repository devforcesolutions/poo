
public class Forno extends Oggetto {
	
	private boolean acceso;

	public Forno(String nome, double peso) {
		super(nome, peso);
		this.acceso = false;
	}

	
	
	public boolean accendi() {
		this.acceso = true;
		return acceso;
	}

	public boolean spegni() {
		this.acceso = false;
		return acceso;
	}

}
