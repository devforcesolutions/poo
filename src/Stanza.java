import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ereditarieta.Persona;

public class Stanza {
	
	protected List<Oggetto> oggetti;
	protected Map<Coordinata, Stanza> coordinataToStanzaAdiacente;
	protected String nome;
	protected List<Persona> personeNellaStanza;

	public Stanza(String nome, Oggetto... oggetti) {
		
		this.personeNellaStanza = new ArrayList<>();
		//NO! un oggetto null � un oggetto NON ISTANZIATO!
		//Ovvero che non hai fatto new, quindi non gli hai dedicato una parte di memoria
		
		//Tu prima andavi in NullPointerException con la casa quando provavi ad aggiungere una stanza, perch� il this.stanze della casa non era istanziato
		//quindi stavi chiamando il metodo add di un oggetto nullo... null.add(qualcosa) va in NullPointerException
		
		if (this.oggetti == null) { //Se la lista oggetti non � istanziata quando creo la stanza...
			this.oggetti = new ArrayList<Oggetto>();	//Istanzio una nuova lista di oggetti vuota. se io levassi sta riga di codice succederebbe la stessa cosa quando inserisco un oggetto
		}
		
		for (Oggetto oggetto : oggetti) {	//Per ogni oggetto passato nel costruttore
			this.oggetti.add(oggetto);	//Aggiungo alla lista l'oggetto i-esimo ok?
		}
		
		this.coordinataToStanzaAdiacente = new HashMap<>(); //ISTANZIARE!!!
	}

	
	
	public String getNome() {
		return nome;
	}

	
	public void setNome(String nome) {
		this.nome = nome;
	}


	
	public List<Persona> getPersoneNellaStanza() {
		return personeNellaStanza;
	}



	public void setPersoneNellaStanza(List<Persona> personeNellaStanza) {
		this.personeNellaStanza = personeNellaStanza;
	}
	
	
	

	public List<Oggetto> getOggetti() {
		return oggetti;
	}

	public void setOggetti(List<Oggetto> oggetti) {
		this.oggetti = oggetti;
	}



	public Map<Coordinata, Stanza> getCoordinataToStanzaAdiacente() {
		return coordinataToStanzaAdiacente;
	}



	public void setCoordinataToStanzaAdiacente(Map<Coordinata, Stanza> coordinataToStanzaAdiacente) {
		this.coordinataToStanzaAdiacente = coordinataToStanzaAdiacente;
	}
	
	public Stanza getStanzaAdiacente(Coordinata coordinata) {
		//Con questo metodo passi una direzione alla stanza e lei ti rid� la stanza adiacente
		Stanza stanzaAdiacente = this.coordinataToStanzaAdiacente.get(coordinata);
		if (stanzaAdiacente != null) {
			return stanzaAdiacente;
		}
		else {
			System.out.println("Non esiste una stanza adiacente alla direzione specificata");
			return null;
		}
	}
	
	

}
