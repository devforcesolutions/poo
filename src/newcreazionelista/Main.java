package newcreazionelista;

import ereditarieta.Persona;

public class Main  {

	public static void main(String[] args) {
		
		Persona p1 = new Persona("Franco", "Pitocchi", 55);
		Persona p2 = new Persona("Marco", "Whitvalley", 24);
		Persona p3 = new Persona("Alessio", "D'annafanculo", 24);
		
		NodoLista<Persona> root = new NodoLista<>();
		
		root.add(p1);
		root.add(p2);
		root.add(p3);
		System.out.println(root.getSize());	
			while(root  != null){
				Persona p = root.getInfo();
				System.out.println(p.getNome() + " " + p.getCognome());
				root = root.getNext();
				
			}

		
	}
}
