package newcreazionelista;

public class NodoLista <T> {
	
	private T info;
	private NodoLista next;
	
	public void add(T t) {
		if(this.info == null) {
			this.info = t;
		}
		else {
			if (this.next == null) {
				this.next = new NodoLista();
			}
			this.next.add(t);
		}
	}

	public T getInfo() {
		return info;
	}

	public void setInfo(T info) {
		this.info = info;
	}

	public NodoLista getNext() {
		return next;
	}

	public void setNext(NodoLista next) {
		this.next = next;
	}
	
	public int getSize() {
		int c = 0;	//METTI GLI SPAZI CANE
		NodoLista posizione = this.next;
		while (posizione != null) {	//METTI GLI SPAZI CANE
			c++;
			posizione = posizione.next; //METTI GLI SPAZI CANE
		}
		return c;
	}
		
	
	
	public T getByIndex(int i) {
		  if(i <= this.getSize()) { //METTI GLI SPAZI CANE
		  int c = 0;
			 while (this != null) {
			 	if (i == c) {
				return this.info;
				}
				else {
					this = this.next;
					c++;
				}
			 }
		  }
		  else {
			System.out.println(" la lista � minore");
		}  
	}

}
