import java.util.ArrayList;
import java.util.List;

import ereditarieta.Persona;

public class Mondo {
	
	private static List<Persona> personeNelMondo = new ArrayList<>();
	
	public static void aggiungiPersona(Persona p) {
		personeNelMondo.add(p);
	}
	public static boolean esistePersonaNelMondo(Persona p) {
		return personeNelMondo.contains(p);
	}

}
